//
//  TopicsViewController.m
//  NursePal
//
//  Created by Rizwan on 2/13/14.
//  Copyright (c) 2014 com. All rights reserved.
//

#import "TopicsViewController.h"
#import "HomeTopicsViewController.h"

@interface TopicsViewController () {
    
    IBOutlet UITableView *tableViewTopic;
    NSArray *arrTopics;
    Loader *loader;
    BOOL topicsIndexes[1000];
    //int counter;
    NSIndexPath *index;
    NSArray *_products;
    IBOutlet UIButton *btnCheckTopic;
    BOOL isProductLoaded;
    EGORefreshTableHeaderView *_refreshHeaderView;
	BOOL _reloading;
    IBOutlet UIView *viewMenu;
    BOOL isRestore;
}

- (IBAction)checkBtnPressed:(id)sender forEvent:(UIEvent *)event;
- (IBAction)signOutBtnPressed:(id)sender;
- (IBAction)okBtnPressed:(id)sender;

- (IBAction)restoreBtnPressed:(id)sender;
- (IBAction)signoutBtnPressed:(id)sender;

@end

@implementation TopicsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    loader = [[Loader alloc] init];
    
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(productPurchased:) name:IAPHelperProductPurchasedNotification object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(cancelPurchased:) name:IAPHelperProductLoader object:@"Failed"];
    //[self reload];
    
    if (NSFoundationVersionNumber > NSFoundationVersionNumber_iOS_6_1)
    {
        self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
        self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
        [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
        self.navigationController.navigationBar.translucent = NO;
        // do stuff for iOS 7 and newer
    }
    else
    {
        [self.navigationController.navigationBar setBarStyle:UIBarStyleBlackOpaque];
    }
    
    if (_refreshHeaderView == nil) {
		
		EGORefreshTableHeaderView *view = [[EGORefreshTableHeaderView alloc] initWithFrame:CGRectMake(0.0f, 0.0f - tableViewTopic.bounds.size.height, self.view.frame.size.width, tableViewTopic.bounds.size.height)];
		view.delegate = self;
		[tableViewTopic addSubview:view];
		_refreshHeaderView = view;
	}
    [_refreshHeaderView refreshLastUpdatedDate];
    isRestore = NO;
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if (!arrTopics) {
        //counter = 0;
        [self getMainTopics];
    }
    //  update the last update date
}

- (void)getMainTopics {
    
    [loader startAnimatingWithViewController:self];
    [self.navigationController.view setUserInteractionEnabled:NO];
    NSString *userId = [[NSUserDefaults standardUserDefaults] objectForKey:@"userId"];
    
    [WebClient callAPI:@"/projects/mobileapps/nursepal/services/index/viewmaintopics" withParams:@{@"user_id": userId}
               success:^(NSDictionary *jsonDict) {
                   [loader stopAnimating];
                   [self.navigationController.view setUserInteractionEnabled:YES];
                   if ([jsonDict[@"status"] isEqualToString:@"SUCCESS"]) {
                       arrTopics = jsonDict[@"records"];
                       //NSLog(@"json dic %@", arrTopics);
                       //BOOL *aShow = malloc(sizeof(BOOL)*c);
                       //topicsIndexes = malloc(sizeof(BOOL) * [arrTopics count]);
                       
                       memset(topicsIndexes, 0, sizeof(BOOL));
                       for (int i = 0; i < [arrTopics count]; i++) {
                           if ([arrTopics[i][@"is_selected"] boolValue])
                           {
                               topicsIndexes[i] = YES;
                               if (!_reloading)
                               {
                                   //counter++;
                               }
                           }
                       }
                       [tableViewTopic reloadData];
                   }
               }
               failure:^(NSString *error) {
                   [loader stopAnimating];
                   [self.navigationController.view setUserInteractionEnabled:YES];
                   UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:nil message:error delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                   [alertView show];
               }];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark --
#pragma mark -- UITableView Datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [arrTopics count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TopicCell"];
    cell.backgroundColor = [UIColor clearColor];
    UILabel *topicName = (UILabel *)[cell.contentView viewWithTag:1000];
    [topicName setText:arrTopics[indexPath.row][@"cat_name"]];
    
    UIButton *btnCheck = (UIButton *)[cell.contentView viewWithTag:1001];
    [btnCheck setSelected:[arrTopics[indexPath.row][@"is_selected"] boolValue]];
    
        if (topicsIndexes[indexPath.row])
        {
            [btnCheck setSelected:YES];
        }
        else
        {
            [btnCheck setSelected:NO];
        }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    UIButton *btnCheck = (UIButton *)[cell.contentView viewWithTag:1001];
    index = indexPath;
    
    int counter = 0;
    
    for (int i = 0; i < [arrTopics count]; i++) {
        if (topicsIndexes[i] == YES) {
            counter ++;
        }
    }
    
//    if (counter>=10)
//    {
//        if (!btnCheck.selected)
//        {
//            if (isProductLoaded)
//            {
//                
//                NSArray * skProducts = _products;
//                SKProduct *currentProduct = nil;
//                for (SKProduct * skProduct in skProducts) {
//                    
//                    if ([arrTopics[indexPath.row][@"cat_name"] rangeOfString:skProduct.localizedTitle].location != NSNotFound) {
//                        currentProduct = skProduct;
//                        break;
//                    }
//                }
//                if (currentProduct != nil) {
//                    [loader startAnimatingWithViewController:self];
//                    [self.navigationController.view setUserInteractionEnabled:NO];
//                    isRestore = NO;
//                    [[RageIAPHelper sharedInstance]buyProduct:currentProduct];
//                }
//            }
//        }
//        else
//        {
//            if (![arrTopics[indexPath.row][@"is_selected"] boolValue])
//            {
//                topicsIndexes[indexPath.row] = NO;
//                //counter --;
//            }
//        }
//    }
//    else
//    {
        if (![arrTopics[indexPath.row][@"is_selected"] boolValue])
        {
            [btnCheck setSelected:![btnCheck isSelected]];
            if ([btnCheck isSelected])
            {
                topicsIndexes[indexPath.row] = YES;
                //counter ++;
            }
            else
            {
                topicsIndexes[indexPath.row] = NO;
                //counter --;
            }
        }
//    }
}

- (IBAction)checkBtnPressed:(id)sender forEvent:(UIEvent *)event {
    //[sender setSelected:![sender isSelected]];
}

- (void)signOutBtnPressed:(id)sender {
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"Are you sure?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"SignOut", nil];
    [alertView show];
    
//    [UIView transitionWithView:viewMenu duration:0.5 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
//        if ([viewMenu alpha] == 1.0) {
//            [viewMenu setAlpha:0.0];
//        }
//        else {
//            [viewMenu setAlpha:1.0];
//        }
//    } completion:^(BOOL finished) {
//        
//    }];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 1) {
        [self signoutBtnPressed:nil];
    }
}

- (void)gotoHomeTopics {
    NSMutableArray *arrSelectedTopics = [NSMutableArray array];
    for (int i = 0; i < [arrTopics count]; i++) {
        if (topicsIndexes[i] == YES) {
            [arrSelectedTopics addObject:[arrTopics[i] copy]];
        }
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        HomeTopicsViewController *htVC = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeTopicsViewController"];
        [htVC setArrTopics:arrSelectedTopics];
        [self.navigationController pushViewController:htVC animated:YES];
    });
}

- (IBAction)okBtnPressed:(id)sender {
    
    BOOL shouldUpdateTopics = NO;
    
    NSMutableArray *arrSelectedTopicsid = [NSMutableArray array];
    for (int i = 0; i < [arrTopics count]; i++) {
        if (topicsIndexes[i] == YES) {
            if (![arrTopics[i][@"is_selected"] boolValue]) {
                shouldUpdateTopics = YES;
            }
            [arrSelectedTopicsid addObject:arrTopics[i][@"cat_id"]];
        }
    }
    if (shouldUpdateTopics) {
        
        NSString *catIds = [arrSelectedTopicsid componentsJoinedByString:@","];
        
        if ([arrSelectedTopicsid count]) {
        
        //http://developer.avenuesocial.com/projects/mobileapps/nursepal/services/index/postUserTopics?user_id=13&cat_id=1,2,3,4,5,6,7
            [loader startAnimatingWithViewController:self];
            [self.navigationController.view setUserInteractionEnabled:NO];
            NSString *userId = [[NSUserDefaults standardUserDefaults] objectForKey:@"userId"];

            [WebClient callAPI:@"/projects/mobileapps/nursepal/services/index/postUserTopics"
                    withParams:@{@"user_id": userId, @"cat_id": catIds}
                       success:^(NSDictionary *jsonDict) {
                           [loader stopAnimating];
                           [self.navigationController.view  setUserInteractionEnabled:YES];
                           if ([jsonDict[@"status"] isEqualToString:@"SUCCESS"])
                           {
                               [self gotoHomeTopics];
                                //counter = 0;
                               [self getMainTopics];
                           }
                       }
                       failure:^(NSString *error) {
                           [loader stopAnimating];
                           [self.navigationController.view  setUserInteractionEnabled:YES];
                           UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:nil message:error delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                           [alertView show];
                       }];
        }
        else {
            UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:nil message:@"Please select atleast one topic" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alertView show];
        }
    }
    else {
        [self gotoHomeTopics];
    }
}

- (IBAction)restoreBtnPressed:(id)sender {
    
    //[loader startAnimatingWithViewController:self];
    isRestore = YES;
    [self signOutBtnPressed:nil];
    [[RageIAPHelper sharedInstance] restoreCompletedTransactions];
}

- (IBAction)signoutBtnPressed:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:^{
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"userId"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }];
}

- (void)reload {
    _products = nil;
    [[RageIAPHelper sharedInstance] requestProductsWithCompletionHandler:^(BOOL success, NSArray *products) {
        
        [self.navigationController.view  setUserInteractionEnabled:YES];
        
        if (success) {
            _products = products;
            
            if ([_products count])
            {
                isProductLoaded = YES;
            }
        }
        else {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"Unable to load In App. Please check your internet" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alertView show];
        }
    }];
}

-(void)productPurchased:(NSNotification *)notification {
    
    NSString * productIdentifier = notification.object;
    [_products enumerateObjectsUsingBlock:^(SKProduct * product, NSUInteger idx, BOOL *stop) {
        
        if ([product.productIdentifier isEqualToString:productIdentifier])
        {
            //NSLog(@"identifier 1 %@, identifier 2 %@", product.productIdentifier, productIdentifier);
            if (!isRestore) {
                
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:[NSString stringWithFormat:@"%@ Purchased Successfuly",[arrTopics objectAtIndex:index.row][@"cat_name"]] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alertView show];
                
                if (![arrTopics[index.row][@"is_selected"] boolValue])
                {
                    topicsIndexes[index.row] = YES;
                    //counter++;
                }
                [self postUserTopics];
            }
            else {
                NSUInteger currrentIndex = 0;
                
                //for (NSDictionary *dic in arrTopics) {
                for (int i = 0; i < [arrTopics count]; i ++) {
                    
                    NSDictionary *dic = arrTopics[i];
                    if ([dic[@"cat_name"] rangeOfString:product.localizedTitle].location != NSNotFound) {
                        currrentIndex = i;
                        break;
                    }
                }
                if (![arrTopics[currrentIndex][@"is_selected"] boolValue])
                {
                    topicsIndexes[currrentIndex] = YES;
                    [self postUserTopics];
                    //counter++;
                }
            }
            
            
            [loader stopAnimating];
            [self.navigationController.view setUserInteractionEnabled:YES];
            *stop = YES;
        }
    }];
    
}
-(void)cancelPurchased:(NSNotification *)notification
{
    [loader stopAnimating];
    [self.navigationController.view setUserInteractionEnabled:YES];
}
-(void)postUserTopics
{
    BOOL shouldUpdateTopics = NO;
    
    NSMutableArray *arrSelectedTopicsid = [NSMutableArray array];
    for (int i = 0; i < [arrTopics count]; i++) {
        if (topicsIndexes[i] == YES) {
            if (![arrTopics[i][@"is_selected"] boolValue]) {
                shouldUpdateTopics = YES;
            }
            [arrSelectedTopicsid addObject:arrTopics[i][@"cat_id"]];
        }
    }
    if (shouldUpdateTopics) {
        
        NSString *catIds = [arrSelectedTopicsid componentsJoinedByString:@","];
        
        if ([arrSelectedTopicsid count]) {
            
            //http://developer.avenuesocial.com/projects/mobileapps/nursepal/services/index/postUserTopics?user_id=13&cat_id=1,2,3,4,5,6,7
            [loader startAnimatingWithViewController:self];
            [self.navigationController.view setUserInteractionEnabled:NO];
            NSString *userId = [[NSUserDefaults standardUserDefaults] objectForKey:@"userId"];
            
            [WebClient callAPI:@"/projects/mobileapps/nursepal/services/index/postUserTopics"
                    withParams:@{@"user_id": userId, @"cat_id": catIds}
                       success:^(NSDictionary *jsonDict) {
                           [loader stopAnimating];
                           [self.navigationController.view setUserInteractionEnabled:YES];
                           if ([jsonDict[@"status"] isEqualToString:@"SUCCESS"])
                           {
                               //counter = 0;
                               [self getMainTopics];
                           }
                       }
                       failure:^(NSString *error) {
                           [loader stopAnimating];
                           [self.navigationController.view setUserInteractionEnabled:YES];
                           UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:nil message:error delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                           [alertView show];
                       }];
        }
        else {
            UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:nil message:@"Please select atleast one topic" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alertView show];
            
        }
    }
}
#pragma mark -
#pragma mark Data Source Loading / Reloading Methods
- (void)reloadTableViewDataSource{
	
	//  should be calling your tableviews data source model to reload
	//  put here just for demo;
    _reloading = YES;
    [self getMainTopics];
}
- (void)doneLoadingTableViewData{
	
	//  model should call this when its done loading
	_reloading = NO;
	[_refreshHeaderView egoRefreshScrollViewDataSourceDidFinishedLoading:tableViewTopic];
	
}
#pragma mark -
#pragma mark UIScrollViewDelegate Methods

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    if ([viewMenu alpha] == 1.0) {
        [self signOutBtnPressed:nil];
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
	
	[_refreshHeaderView egoRefreshScrollViewDidScroll:scrollView];
    
}
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
	
	[_refreshHeaderView egoRefreshScrollViewDidEndDragging:scrollView];
	
}

#pragma mark -
#pragma mark EGORefreshTableHeaderDelegate Methods

- (void)egoRefreshTableHeaderDidTriggerRefresh:(EGORefreshTableHeaderView*)view{
	
	[self reloadTableViewDataSource];
	[self performSelector:@selector(doneLoadingTableViewData) withObject:nil afterDelay:3.0];
	
}
- (BOOL)egoRefreshTableHeaderDataSourceIsLoading:(EGORefreshTableHeaderView*)view{
	
	return _reloading; // should return if data source model is reloading
	
}
- (NSDate*)egoRefreshTableHeaderDataSourceLastUpdated:(EGORefreshTableHeaderView*)view{
	
	return [NSDate date]; // should return date data source was last changed
	
}
@end
