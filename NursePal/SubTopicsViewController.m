//
//  TopicContentViewController.m
//  NursePal
//
//  Created by Rizwan on 2/13/14.
//  Copyright (c) 2014 com. All rights reserved.
//

#import "SubTopicsViewController.h"
#import "TopicDetailViewController.h"

@interface SubTopicsViewController () {
    
    IBOutlet UITableView *tableViewTopicContent;
    NSArray *arrSubTopics;
    Loader *loader;
    
}

@end

@implementation SubTopicsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	loader = [[Loader alloc] init];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if (!arrSubTopics) {
        [self getSubTopics];
    }
}

- (void)getSubTopics {
    
    [loader startAnimatingWithViewController:self];
    
    [WebClient callAPI:@"/projects/mobileapps/nursepal/services/index/viewinnertopics" withParams:@{@"topic_id": @(_topicId)}
               success:^(NSDictionary *jsonDict) {
                   [loader stopAnimating];
                   
                   if ([jsonDict[@"status"] isEqualToString:@"SUCCESS"]) {
                       arrSubTopics = jsonDict[@"data"];
                       [tableViewTopicContent reloadData];
                   }
               }
               failure:^(NSString *error) {
                   [loader stopAnimating];
                   UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:nil message:error delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                   [alertView show];
               }];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark --
#pragma mark -- UITableView Datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [arrSubTopics count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TopicContentCell"];
    cell.backgroundColor = [UIColor clearColor];
    UILabel *topicName = (UILabel *)[cell.contentView viewWithTag:1000];
    [topicName setText:arrSubTopics[indexPath.row][@"sub_cat_name"]];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    [super prepareForSegue:segue sender:sender];
    
    //UITableViewCell *cell = (UITableViewCell *)sender;
    //NSIndexPath *indexPath = [tableViewTopicContent indexPathForCell:cell];
    NSIndexPath *indexPath = [tableViewTopicContent indexPathForSelectedRow];
    
    TopicDetailViewController *tcdVC = [segue destinationViewController];
    [tcdVC setTopicId:_topicId];
    [tcdVC setSubTopicId:[arrSubTopics[indexPath.row][@"sub_cat_id"] intValue]];
    [tcdVC setTitle:arrSubTopics[indexPath.row][@"sub_cat_name"]];
}

@end
