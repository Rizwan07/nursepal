//
//  WebClient.h
//  HapiMomi
//
//  Created by Erik Villegas on 12/17/12.
//  Copyright (c) 2012 Dobango. All rights reserved.
//

#import <Foundation/Foundation.h>

//
//#define IMAGE_PATH @"imagePath"
//#define IMAGE_VIEW @"imageView"
//#define IMAGE_DATA @"imageData"
//#define IMAGE_INFO @"imageInfo"
//
//#define PICTURE_ARRAY @"pictureArray"
//#define DATA_ARRAY @"dataArray"
//
//#define WALLPOST @"wallPost"
//#define MEMBER @"member"


@interface WebClient : NSObject

+ (void)callAPI:(NSString *) api withParams:(NSDictionary *)params success:(void (^)(NSDictionary *jsonDict))success failure:(void (^)(NSString *error))failure;

@end
