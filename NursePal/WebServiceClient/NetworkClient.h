//
//  HMNetworkClient.h
//  HapiMomi
//
//  Created by Erik Villegas on 12/31/12.
//  Copyright (c) 2012 Dobango. All rights reserved.
//

#import "AFHTTPClient.h"

@interface NetworkClient : AFHTTPClient

+ (NetworkClient *)sharedClient;

@end
