//
//  HMNetworkClient.m
//  HapiMomi
//
//  Created by Erik Villegas on 12/31/12.
//  Copyright (c) 2012 Dobango. All rights reserved.
//

#import "NetworkClient.h"
#import "AFJSONRequestOperation.h"
#import "AFNetworkActivityIndicatorManager.h"

#define baseUrl @"http://developer.avenuesocial.com/projects/mobileapps/nursepal/services/index/"

@implementation NetworkClient

+ (NetworkClient *)sharedClient {
    
    static NetworkClient *_sharedClient = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedClient = [[NetworkClient alloc] initWithBaseURL:[NSURL URLWithString:baseUrl]];
    });
    
    [[AFNetworkActivityIndicatorManager sharedManager] setEnabled:YES];
    
    return _sharedClient;
}

- (id)initWithBaseURL:(NSURL *)url {
    
    if (self = [super initWithBaseURL:url]) {
        
        [self registerHTTPOperationClass:[AFJSONRequestOperation class]];
        [self setDefaultHeader:@"Accept" value:@"application/json"];
    
    }

    return self;
}

@end