//
//  WebClient.m
//  HapiMomi
//
//  Created by Erik Villegas on 12/17/12.
//  Copyright (c) 2012 Dobango. All rights reserved.
//

#import "WebClient.h"
#import "NetworkClient.h"
#import "AFHTTPRequestOperation.h"

@implementation WebClient

// This method will call the provided API using the POST HTTP method
/* api: The API to be called. Note this is not the full API, but only the unique portion.
 * params: The parameters to be sent with the request
 * success: The block to be invoked when the API returns successfully. The parsed JSON Dictionary is sent as a parameter.
 * failure: The block to be invoked when the API returns unsuccessfully. The approprite error is sent as a parameter.
 */
+ (void)callAPI:(NSString *) api withParams:(NSDictionary *)params success:(void (^)(NSDictionary *jsonDict))success failure:(void (^)(NSString *error))failure {

    NSLog(@"%@ request: %@", api, params);

    void (^successBlock)(AFHTTPRequestOperation *operation, id JSON);
    void (^failureBlock)(AFHTTPRequestOperation *operation, NSError *error);

    successBlock = ^(AFHTTPRequestOperation *operation, id JSON) {

        NSLog(@"successfully called API %@", operation.request.URL);

        if (success) {
            success(JSON);
        }
    };

    failureBlock = ^(AFHTTPRequestOperation *operation, NSError *error) {

        failure(error.localizedDescription);
    };

    [[NetworkClient sharedClient] postPath:api parameters:params success:successBlock failure:failureBlock];
//    [[HMNetworkClient sharedClient] getPath:api parameters:params success:successBlock failure:failureBlock];
}

@end
