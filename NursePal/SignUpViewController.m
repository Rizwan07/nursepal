//
//  SignUpViewController.m
//  NursePal
//
//  Created by Rizwan on 2/13/14.
//  Copyright (c) 2014 com. All rights reserved.
//

#import "SignUpViewController.h"
#import "Singleton.h"
@interface SignUpViewController () {
    
    IBOutlet UITextField *textFieldEmail;
    IBOutlet UITextField *textFieldPassword;
    IBOutlet UITextField *textFieldConfirmPassword;
    Loader *loader;
    Singleton *single;
}

- (IBAction)signUpBtnPressed:(id)sender;

@end

@implementation SignUpViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	loader = [[Loader alloc] init];
    single = [Singleton sharedSingleton];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)signUpBtnPressed:(id)sender {
    
    //http://developer.avenuesocial.com/projects/mobileapps/nursepal/services/index/signup?
    //email=fahad2@gmail.com&password=123
    
    if ([textFieldEmail.text length] > 0 && [textFieldPassword.text length] > 0 && [textFieldConfirmPassword.text length] > 0) {
        if ([single method_NSStringIsValidEmail:textFieldEmail.text] == true)
        {
            if ([textFieldPassword.text isEqualToString:[textFieldConfirmPassword text]]) {
                
                [loader startAnimatingWithViewController:self];
                
                [WebClient callAPI:@"/projects/mobileapps/nursepal/services/index/signup" withParams:@{ @"email": textFieldEmail.text, @"password": textFieldPassword.text }
                           success:^(NSDictionary *jsonDict) {
                               [loader stopAnimating];
                               if ([jsonDict[@"status"] isEqualToString:@"SUCCESS"]) {
                                   
                                   NSString *userId = jsonDict[@"data"][@"user_id"];
                                   [[NSUserDefaults standardUserDefaults] setObject:userId forKey:@"userId"];
                                   [[NSUserDefaults standardUserDefaults] synchronize];
                                   
                                   [textFieldEmail setText:@""];
                                   [textFieldPassword setText:@""];
                                   UINavigationController *navController = [self.storyboard instantiateViewControllerWithIdentifier:@"TopicNav"];
                                   [self presentViewController:navController animated:YES completion:^{
                                       [self.navigationController popToRootViewControllerAnimated:YES];
                                   }];
                               }
                               else {
                                   UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:jsonDict[@"msg"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                                   [alert show];
                               }
                           }
                           failure:^(NSString *error) {
                               [loader stopAnimating];
                               UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:nil message:error delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                               [alertView show];
                           }];
            }
            else {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"Password not match" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alertView show];
            }
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Failure" message:@"Email address is not valid." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }
        
    }
    else {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"All Fields required" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertView show];
        
    }
}
#pragma mark --
#pragma mark -- UITextField Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(textField == textFieldEmail)
    {
        [textFieldPassword becomeFirstResponder];
        return NO;
    }
    else if(textField == textFieldPassword)
    {
        [textFieldConfirmPassword becomeFirstResponder];
        return NO;
    }
    else if(textField == textFieldConfirmPassword)
    {
        [textFieldConfirmPassword resignFirstResponder];
        return NO;
    }
    return YES;
}
@end
