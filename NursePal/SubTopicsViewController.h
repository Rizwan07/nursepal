//
//  TopicContentViewController.h
//  NursePal
//
//  Created by Rizwan on 2/13/14.
//  Copyright (c) 2014 com. All rights reserved.
//

#import "ViewController.h"

@interface SubTopicsViewController : ViewController

@property (nonatomic, assign) int topicId;

@end
