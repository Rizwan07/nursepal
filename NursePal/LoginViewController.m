//
//  LoginViewController.m
//  NursePal
//
//  Created by Rizwan on 2/13/14.
//  Copyright (c) 2014 com. All rights reserved.
//

#import "LoginViewController.h"
#import "TopicsViewController.h"
#import "Singleton.h"
@interface LoginViewController () {
    
    IBOutlet UITextField *textFieldEmail;
    IBOutlet UITextField *textFieldPassword;
    IBOutlet UIButton *btnRememberMe;
    Singleton *single;
    Loader *loader;
}

- (IBAction)rememberMeBtnPressed:(id)sender;
- (IBAction)LoginBtnPressed:(id)sender;
- (IBAction)ForgotPasswordBtnPressed:(id)sender;
- (IBAction)loginWithoutRegisterBtnPressed:(id)sender;

- (IBAction)unwindViewController:(UIStoryboardSegue *)sender;

@end

@implementation LoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    loader = [[Loader alloc] init];
    single = [Singleton sharedSingleton];
	if ([[NSUserDefaults standardUserDefaults] objectForKey:@"userId"]) {
        [self gotoNextVC:NO];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)unwindViewController:(UIStoryboardSegue *)sender {
}

- (IBAction)rememberMeBtnPressed:(id)sender {
    
    [btnRememberMe setSelected:![btnRememberMe isSelected]];
}

- (IBAction)LoginBtnPressed:(id)sender {
    
    
    //http://developer.avenuesocial.com/projects/mobileapps/nursepal/services/index/login?
    //email=fahad2@gmail.com&password=123
    
    if ([textFieldEmail.text length] > 0 && [textFieldPassword.text length] > 0)
    {
        if ([single method_NSStringIsValidEmail:textFieldEmail.text] == true)
        {
            [loader startAnimatingWithViewController:self];
            
            [WebClient callAPI:@"/projects/mobileapps/nursepal/services/index/login" withParams:@{ @"email": textFieldEmail.text, @"password": textFieldPassword.text }
                       success:^(NSDictionary *jsonDict) {
                           [loader stopAnimating];
                           if ([jsonDict[@"status"] isEqualToString:@"SUCCESS"]) {
                               
                               NSString *userId = jsonDict[@"data"][@"user_id"];
                               [[NSUserDefaults standardUserDefaults] setObject:userId forKey:@"userId"];
                               [[NSUserDefaults standardUserDefaults] synchronize];
                               
                               [textFieldEmail setText:@""];
                               [textFieldPassword setText:@""];
                               [self gotoNextVC:YES];
                           }
                           else {
                               UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:jsonDict[@"msg"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                               [alert show];
                           }
                       }
                       failure:^(NSString *error) {
                           [loader stopAnimating];
                           UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:nil message:error delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                           [alertView show];
                       }];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Failure" message:@"Email address is not valid." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }
    }
    else {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"All Fields required" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertView show];

    }
}

- (IBAction)ForgotPasswordBtnPressed:(id)sender {
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"Please enter your Email" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
    [alertView setAlertViewStyle:UIAlertViewStylePlainTextInput];
    [[alertView textFieldAtIndex:0] setKeyboardType:UIKeyboardTypeEmailAddress];
    [alertView show];
}

- (IBAction)loginWithoutRegisterBtnPressed:(id)sender {
    
    NSString *str = [self getSecureValueForKey:@"DeviceUDIDForKeyChain"];
    if (!str) {
        str = [[[[UIDevice currentDevice] identifierForVendor] UUIDString] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        [self storeSecureValue:str forKey:@"DeviceUDIDForKeyChain"];
    }
    
    [loader startAnimatingWithViewController:self];
    
    [WebClient callAPI:@"/projects/mobileapps/nursepal/services/index/signupViaDeviceId" withParams:@{ @"deviceID": str }
               success:^(NSDictionary *jsonDict) {
                   [loader stopAnimating];
                   if ([jsonDict[@"status"] isEqualToString:@"SUCCESS"]) {
                       
                       NSString *userId = jsonDict[@"data"][@"user_id"];
                       [[NSUserDefaults standardUserDefaults] setObject:userId forKey:@"userId"];
                       [[NSUserDefaults standardUserDefaults] synchronize];
                       
                       [textFieldEmail setText:@""];
                       [textFieldPassword setText:@""];
                       [self gotoNextVC:YES];
                   }
                   else {
                       UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:jsonDict[@"msg"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                       [alert show];
                   }
               }
               failure:^(NSString *error) {
                   [loader stopAnimating];
                   UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:nil message:error delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                   [alertView show];
               }];

}

- (void)gotoNextVC:(BOOL)animated {
    
    UINavigationController *navController = [self.storyboard instantiateViewControllerWithIdentifier:@"TopicNav"];
    [self presentViewController:navController animated:animated completion:nil];
}


#pragma mark --
#pragma mark -- UIAlertView Delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    NSString *strEmail = [[alertView textFieldAtIndex:0] text];
    if (buttonIndex == 1 && strEmail.length != 0) {
        
        //http://developer.avenuesocial.com/projects/mobileapps/nursepal/services/index/forgotpassword?
        
        [loader startAnimatingWithViewController:self];
        
        [WebClient callAPI:@"/projects/mobileapps/nursepal/services/index/forgotpassword" withParams:@{ @"email": strEmail }
                   success:^(NSDictionary *jsonDict) {
                       [loader stopAnimating];
                       
                       UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:jsonDict[@"msg"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                       [alert show];
                   }
                   failure:^(NSString *error) {
                       [loader stopAnimating];
                       UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:nil message:error delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                       [alertView show];
                   }];
        
    }
    
}

-(NSString *)getSecureValueForKey:(NSString *)key {
    /*
     
     Return a value from the keychain
     
     */
    
    // Retrieve a value from the keychain
    NSDictionary *result;
    NSArray *keys = [[NSArray alloc] initWithObjects: (__bridge NSString *) kSecClass, kSecAttrAccount, kSecReturnAttributes, nil];
    NSArray *objects = [[NSArray alloc] initWithObjects: (__bridge NSString *) kSecClassGenericPassword, key, kCFBooleanTrue, nil];
    NSDictionary *query = [[NSDictionary alloc] initWithObjects: objects forKeys: keys];
    
    // Check if the value was found
    OSStatus status = SecItemCopyMatching((__bridge CFDictionaryRef) query, (CFTypeRef *)(void *) &result);
    
    if (status != noErr) {
        // Value not found
        return nil;
    } else {
        // Value was found so return it
        NSString *value = (NSString *) [result objectForKey: (__bridge NSString *) kSecAttrGeneric];
        return value;
    }
}




// -------------------------------------------------------------------------
-(bool)storeSecureValue:(NSString *)value forKey:(NSString *)key {
    /*
     
     Store a value in the keychain
     
     */
    
    // Get the existing value for the key
    NSString *existingValue = [self getSecureValueForKey:key];
    
    // Check if a value already exists for this key
    OSStatus status;
    if (existingValue) {
        // Value already exists, so update it
        NSArray *keys = [[NSArray alloc] initWithObjects: (__bridge NSString *) kSecClass, kSecAttrAccount, nil];
        NSArray *objects = [[NSArray alloc] initWithObjects: (__bridge NSString *) kSecClassGenericPassword, key, nil];
        NSDictionary *query = [[NSDictionary alloc] initWithObjects: objects forKeys: keys];
        status = SecItemUpdate((__bridge CFDictionaryRef) query, (__bridge CFDictionaryRef) [NSDictionary dictionaryWithObject:value forKey: (__bridge NSString *) kSecAttrGeneric]);
    } else {
        // Value does not exist, so add it
        NSArray *keys = [[NSArray alloc] initWithObjects: (__bridge NSString *) kSecClass, kSecAttrAccount, kSecAttrGeneric, nil];
        NSArray *objects = [[NSArray alloc] initWithObjects: (__bridge NSString *) kSecClassGenericPassword, key, value, nil];
        NSDictionary *query = [[NSDictionary alloc] initWithObjects: objects forKeys: keys];
        status = SecItemAdd((__bridge CFDictionaryRef) query, NULL);
    }
    
    // Check if the value was stored
    if (status != noErr) {
        // Value was not stored
        return false;
    } else {
        // Value was stored
        return true;
    }
}


#pragma mark --
#pragma mark -- UITextField Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(textField == textFieldEmail)
    {
        [textFieldPassword becomeFirstResponder];
        return NO;
    }
    else if(textField == textFieldPassword)
    {
        [textFieldPassword resignFirstResponder];
        return NO;
    }
    return YES;
}

@end
