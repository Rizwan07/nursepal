//
//  TopicContentDetailViewController.h
//  NursePal
//
//  Created by Rizwan on 2/13/14.
//  Copyright (c) 2014 com. All rights reserved.
//

#import "ViewController.h"

@interface TopicDetailViewController : ViewController

@property (nonatomic, assign) int topicId;
@property (nonatomic, assign) int subTopicId;

@end
