//
//  RageIAPHelper.m
//  In App Rage
//
//  Created by Ray Wenderlich on 9/5/12.
//  Copyright (c) 2012 Razeware LLC. All rights reserved.
//







#import "RageIAPHelper.h"

@implementation RageIAPHelper

+ (RageIAPHelper *)sharedInstance {
    static dispatch_once_t once;
    static RageIAPHelper * sharedInstance;
    dispatch_once(&once, ^{
        NSSet * productIdentifiers = [NSSet setWithObjects:
                                      @"com.nursepal.Dehydration",
                                      @"com.nursepal.Pain",
                                      @"com.nursepal.Coronary.Artery.Disease",
                                      @"com.nursepal.Peripheral.Vascular.Disease",
                                      @"com.nursepal.Pressure.Ulcers",
                                      @"com.nursepal.Urinary.Tract.Infection",
                                      @"com.nursepal.Lumbago",
                                      @"com.nursepal.Hypothyroidism",
                                      @"com.nursepal.Failure.To.Thrive",
                                      @"com.nursepal.Obesity",
                                      @"com.nursepal.Multiple.Sclerosis",
                                      @"com.nursepal.Lymphedema",
                                      @"com.nursepal.Parkinsons.Disease",
                                      @"com.nursepal.About",
                                      @"com.nursepal.Anemia",
                                      @"com.nursepal.After.Care.Surgery",
                                      @"com.nursepal.Arthritis",
                                      @"com.nursepal.Diabetes",
                                      @"com.nursepal.Alzheimers",
                                      @"com.nursepal.Hypertension",
                                      @"com.nursepal.CHF",
                                      @"com.nursepal.CVA",
                                      @"com.nursepal.Cirrhosis",
                                      @"com.nursepal.COPD",
                                      @"com.nursepal.GERD",
                                      @"com.nursepal.Hyperlipidemia",
                                      @"com.nursepal.ESRD",
                                      //@"com.nursepal.topic",
                                      nil];
        sharedInstance = [[self alloc] initWithProductIdentifiers:productIdentifiers];
    });
    return sharedInstance;
}

@end
