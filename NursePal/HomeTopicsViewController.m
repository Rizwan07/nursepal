//
//  HomeViewController.m
//  NursePal
//
//  Created by Rizwan on 2/13/14.
//  Copyright (c) 2014 com. All rights reserved.
//

#import "HomeTopicsViewController.h"
#import "SubTopicsViewController.h"

@interface HomeTopicsViewController () {
    
    IBOutlet UITableView *tableViewHome;
}

@end

@implementation HomeTopicsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark --
#pragma mark -- UITableView Datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_arrTopics count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HomeCell"];
    cell.backgroundColor = [UIColor clearColor];
    UILabel *topicName = (UILabel *)[cell.contentView viewWithTag:1000];
    [topicName setText:_arrTopics[indexPath.row][@"cat_name"]];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    [super prepareForSegue:segue sender:sender];
    
    UITableViewCell *cell = (UITableViewCell *)sender;
    NSIndexPath *indexPath = [tableViewHome indexPathForCell:cell];
    
    SubTopicsViewController *stVC = segue.destinationViewController;
    [stVC setTopicId:[_arrTopics[indexPath.row][@"cat_id"] intValue]];
    [stVC.navigationItem setTitle:_arrTopics[indexPath.row][@"cat_name"]];
}

@end
