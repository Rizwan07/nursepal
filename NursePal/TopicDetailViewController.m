//
//  TopicContentDetailViewController.m
//  NursePal
//
//  Created by Rizwan on 2/13/14.
//  Copyright (c) 2014 com. All rights reserved.
//

#import "TopicDetailViewController.h"

@interface TopicDetailViewController () {
    
    IBOutlet UIScrollView *scrollView;
    IBOutlet UIImageView *imageView;
    IBOutlet UILabel *lblContentDetail;
    IBOutlet NSLayoutConstraint *lblConstraintHeight;
    
    NSDictionary *dic;
    
    IBOutlet UIActivityIndicatorView *imageIndicatorView;
    Loader *loader;
}

@end

@implementation TopicDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    loader = [[Loader alloc] init];
    
    [imageView setImage:nil];
    
////    [self.view removeConstraints:[self.view constraints]];
////    [scrollView removeConstraints:[scrollView constraints]];
////    [lblContentDetail removeConstraints:[lblContentDetail constraints]];
////    [imageView removeConstraints:[imageView constraints]];
////    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"|[scrollView]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(scrollView)]];
////    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[scrollView]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(scrollView)]];
////    [scrollView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"|-(10)-[imageView(300)]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(imageView)]];
////    [scrollView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(10)-[imageView(200)]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(imageView)]];
//
//    [scrollView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"|-(15)-[lblContentDetail(290)]|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(lblContentDetail)]];
//    [scrollView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:strFormate options:0 metrics:nil views:NSDictionaryOfVariableBindings(lblContentDetail)]];

}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self getTopicDetail];
}


- (void)getTopicDetail {
    
    [loader startAnimatingWithViewController:self];
    
    [WebClient callAPI:@"/projects/mobileapps/nursepal/services/index/viewtopicdetail" withParams:@{@"topic_id": @(_topicId), @"inner_topic_id": @(_subTopicId)}
               success:^(NSDictionary *jsonDict) {
                   [loader stopAnimating];
                   if ([jsonDict[@"status"] isEqualToString:@"SUCCESS"]) {
                       dic = jsonDict[@"data"][0];
                       [self setConstraints];
                       [imageIndicatorView startAnimating];

                       NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:dic[@"image"]]];
                       [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
                           if (data && !connectionError) {
                               UIImage *image = [UIImage imageWithData:data];
                               [imageView setImage:image];
                           }
                           [imageIndicatorView stopAnimating];
                       }];
                       
                   }
               }
               failure:^(NSString *error) {
                   [loader stopAnimating];
                   UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:nil message:error delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                   [alertView show];
               }];
    
}

- (void)setConstraints {
    
    [lblContentDetail setText:dic[@"detail"]];
    
    CGSize size = [lblContentDetail.text sizeWithFont:lblContentDetail.font constrainedToSize:CGSizeMake(lblContentDetail.frame.size.width, CGFLOAT_MAX) lineBreakMode:lblContentDetail.lineBreakMode];
    
    lblConstraintHeight.constant = size.height;
    
//    NSString *strConstraintH = [NSString stringWithFormat:@"|-(%.0f)-[lblContentDetail(%.0f)]|", lblContentDetail.frame.origin.x, lblContentDetail.frame.size.width];
//    NSString *strConstraintV = [NSString stringWithFormat:@"V:|-(%.0f)-[lblContentDetail(%.0f)]|",imageView.frame.size.height+20, size.height+20];
//    //NSString *strConstraintV = [NSString stringWithFormat:@"V:|-[imageView]-(%d)-[lblContentDetail(%.0f)]|", 20, size.height+20];
//    
//    [scrollView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:strConstraintH options:0 metrics:nil views:NSDictionaryOfVariableBindings(lblContentDetail)]];
//    [scrollView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:strConstraintV options:0 metrics:nil views:NSDictionaryOfVariableBindings(lblContentDetail)]];

}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
