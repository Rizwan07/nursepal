//
//  Singleton.m
//  Alarm App
//
//  Created by Rizwan on 12/19/13.
//  Copyright (c) 2013 Salsoft Technologies. All rights reserved.
//

#import "Singleton.h"

#import "MZLoadingCircle.h"


@implementation Singleton

static Singleton *single = nil;

+ (Singleton *)sharedSingleton {
    
    if (single == nil) {
        single = [[Singleton alloc] init];
    }
    return single;
}
#pragma mark - is email address validity?
-(BOOL)method_NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = YES; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSString *laxString = @".+@.+\\.[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}
@end