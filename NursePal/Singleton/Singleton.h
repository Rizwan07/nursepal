//
//  Singleton.h
//  Alarm App
//
//  Created by Rizwan on 12/19/13.
//  Copyright (c) 2013 Salsoft Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Singleton : NSObject

+ (Singleton *)sharedSingleton;
-(BOOL) method_NSStringIsValidEmail:(NSString *)checkString;
@end
