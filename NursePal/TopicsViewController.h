//
//  TopicsViewController.h
//  NursePal
//
//  Created by Rizwan on 2/13/14.
//  Copyright (c) 2014 com. All rights reserved.
//

#import "ViewController.h"
#import "RageIAPHelper.h"
#import "EGORefreshTableHeaderView.h"
@interface TopicsViewController : ViewController<EGORefreshTableHeaderDelegate>

@end
