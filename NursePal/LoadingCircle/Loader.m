//
//  Loader.m
//  NursePal
//
//  Created by Rizwan on 3/4/14.
//  Copyright (c) 2014 com. All rights reserved.
//

#import "Loader.h"
#import "MZLoadingCircle.h"

@interface Loader () {
    MZLoadingCircle *loadingCircle;
}

@end

@implementation Loader


- (void)startAnimatingWithViewController:(UIViewController *)vc {
    if (!loadingCircle) {
        loadingCircle = [[MZLoadingCircle alloc]initWithNibName:nil bundle:nil];
        loadingCircle.view.backgroundColor = [UIColor clearColor];
        
        //Colors for layers
        loadingCircle.colorCustomLayer = [UIColor colorWithRed:0 green:0.4 blue:0 alpha:1];
        loadingCircle.colorCustomLayer2 = [UIColor colorWithRed:0 green:0.4 blue:0 alpha:0.65];
        loadingCircle.colorCustomLayer3 = [UIColor colorWithRed:218.0f/255.0f green:139.0f/255.0f blue:38.0f/255.0f alpha:1.0f];//[UIColor colorWithRed:0 green:0.4 blue:0 alpha:0.4];
        
        int size = 100;
        
        CGRect frame = loadingCircle.view.frame;
        frame.size.width = size ;
        frame.size.height = size;
        frame.origin.x = vc.view.frame.size.width / 2 - frame.size.width / 2;
        frame.origin.y = vc.view.frame.size.height / 2 - frame.size.height / 2;
        loadingCircle.view.frame = frame;
        loadingCircle.view.layer.zPosition = MAXFLOAT;
    }
    [vc.view addSubview: loadingCircle.view];
    
}

- (void)stopAnimating {
    if (loadingCircle) {
        
        [loadingCircle.view removeFromSuperview];
    }
}

@end
